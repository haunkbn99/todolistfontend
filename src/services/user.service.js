import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/';
const API_URL_TODO = 'http://localhost:8080/api/todos/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'test/all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'test/user', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'test/admin', { headers: authHeader() });
  }

  async getTodos() {
    try {
      const res = await axios.get(API_URL_TODO, { headers: authHeader() });
      return {
        status: 200,
        data: res.data,
      }
    } catch (err) {
      return {
        status: 400,
        error: err,
      }
    }
  }

  createTodo(todo) {
    return axios.post(API_URL_TODO, todo, { headers: authHeader() })
  }

  deleteTodo(id) {
    return axios.delete(`${API_URL_TODO}${id}`, { headers: authHeader() })
  }

  updateDoneStatus(id) {
    const api = `${API_URL_TODO}${id}/completed`
    return axios.post(api, {}, { headers: authHeader() })
  }
}

export default new UserService();
